#!/usr/bin/env bash
set -e

read MESSAGE
echo "[LISTENER] ==========> Checking for entry '$MESSAGE' in config file..."
if grep -Fxq "replicaof $MESSAGE" /etc/keydb/keydb.conf
then
    echo "[LISTENER] ==========> 'replicaof $MESSAGE' already configured"
else
    keydb-cli replicaof ${MESSAGE} || echo "replicaof ${MESSAGE}" >> /etc/keydb/keydb.conf
    keydb-cli config rewrite || echo "[LISTENER] ==========> KeyDB server is not active yet. Config file at /etc/keydb/keydb.conf was updated manually."
    echo "[LISTENER] ==========> Registered new master at '$MESSAGE'"
fi

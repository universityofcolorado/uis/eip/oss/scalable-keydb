#!/usr/bin/env bash

set -e

# CALLED WHEN CONTAINER IS SHUT DOWN (HOPEFULLY)
cleanup(){
    echo "Cleanup, cleanup, everybody everywhere!"
    for i in ${EXISTING_CONTAINERS[@]}; do
        echo "******** $THIS_CONTAINER_NAME HAS LEFT THE BUILDING *********" | netcat ${i} 7777
    done
}

# TRY TO TRAP SOME SIGNALS
trap 'true' SIGTERM SIGINT

echo "Starting custom entrypoint..."

# LAUNCH LISTENER
echo "Launching listener in background on port ${LISTENER_PORT}..."
( socat -u tcp-l:${LISTENER_PORT},fork system:listener.sh ) &

# INIT SOME VARIABLES FOR LATER USE
RANCHER_STACK_NAME=$(curl -s --header "Accept: application/json" http://rancher-metadata/latest/self | jq -r '.stack.name')
RANCHER_SERVICE_NAME=$(curl -s --header "Accept: application/json" http://rancher-metadata/latest/self | jq -r '.service.name')
EXISTING_CONTAINERS=$(curl -s --header "Accept: application/json" http://rancher-metadata/latest/self/stack/services | jq -r --arg service ${RANCHER_SERVICE_NAME} '.[] | select(.name==$service) | .containers[].name')
THIS_CONTAINER_NAME=$(curl -s --header "Accept: application/json" http://rancher-metadata/latest/self | jq -r '.container.name')

cat <<EOF
***********************************
Current Environment Information
-----------------------------------

Stack Name:
${RANCHER_STACK_NAME}

Service Name:
${RANCHER_SERVICE_NAME}

Existing Containers:
${EXISTING_CONTAINERS[*]}

This Container Name:
${THIS_CONTAINER_NAME}

KeyDB Listening Port:
${KEYDB_PORT}

***********************************
EOF

echo "port ${KEYDB_PORT}" >> /etc/keydb/keydb.conf
echo "requirepass \"${KEYDB_PWD}\"" >> /etc/keydb/keydb.conf
#echo "masterauth \"${KEYDB_PWD}\"" >> /etc/keydb/keydb.conf

# FOR EACH AVAILABLE CONTAINER, SET IT AS A MASTER
for i in ${EXISTING_CONTAINERS[@]}; do
    # BUT ONLY IF IT'S NOT *THIS* CONTAINER!
    if [[ ${i} != ${THIS_CONTAINER_NAME} ]]; then
        # GET THE CONTAINER STATE
        STATE=$(curl -s --header "Accept: application/json" http://rancher-metadata/latest/self/stack/services | jq -r --arg container ${i} --arg service ${RANCHER_SERVICE_NAME} '.[] | select(.name==$service) | .containers[] | select(.name==$container) | .state')
        # IF IT'S RUNNING OR STARTING, WE'RE GOOD TO GO
        if [[ ${STATE} =~ "running" || ${STATE} =~ "starting" ]]; then
            # IF IT'S ALREADY IN OUR CONFIG FILE, DON'T ADD IT AGAIN
            if grep -Fxq "replicaof ${i} ${KEYDB_PORT}" /etc/keydb/keydb.conf; then
                echo "Master ${i} is already configured for replication"
            # OTHERWISE, TRY TO ADD IT
            else
                # NOTIFY THE REMOTE CONTAINER OF *THIS* CONTAINER'S EXISTENCE TO BE ADDED BY IT AS A MASTER AS WELL
                echo "${THIS_CONTAINER_NAME} ${KEYDB_PORT}" | netcat ${i} ${LISTENER_PORT} || echo "[ERROR] Failed to notify container ${i} of my existence! It may not be running yet."
                # ONE LAST CHECK TO SEE IF IT'S ALREADY IN THE CONFIG FILE (JUST IN CASE IT WAS ADDED IN THE MEANTIME) AND APPEND IT TO THE FILE
                if ( ! grep -Fxq "replicaof ${i} ${KEYDB_PORT}" /etc/keydb/keydb.conf ); then
                    echo "replicaof ${i} ${KEYDB_PORT}" >> /etc/keydb/keydb.conf
                fi
                echo "${i} added to /etc/keydb/keydb.conf as a new master"
            fi
        else
            echo "$i state is \"${STATE}\" - skipping"
        fi
    else
        echo "${i} - skipping 'cause it's ME! :)"
    fi
done

echo "Configuration complete. We now return you to your regularly scheduled entrypoint...."

( docker-entrypoint.sh "$@" ) &

echo "Idling while waiting for keydb to exit..."

wait $!

cleanup

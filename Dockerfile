ARG KEYDB_TAG=latest
FROM eqalpha/keydb:$KEYDB_TAG
ARG KEYDB_PORT=6379
COPY entrypoint.sh /usr/local/bin/
COPY listener.sh /usr/local/bin/
COPY keydb.conf /etc/keydb/
RUN \
    chmod +x /usr/local/bin/*.sh && \
    apt-get update && \
    apt-get install -y --no-install-recommends \
        curl \
        jq \
        socat \
        netcat \
    && \
    rm -rf /var/lib/apt/lists/*

ENTRYPOINT ["bash","entrypoint.sh"]

EXPOSE $KEYDB_PORT
CMD ["keydb-server", "/etc/keydb/keydb.conf"]
